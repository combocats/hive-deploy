var events = {
    ADD_FRIEND       : 'addfriend'
};
var feedUserTypes = {
    STANDARD: 'standard',
    DEVELOPER: 'developer'
};

module.exports = {
    prod: {
        port: 3001,
        host: "{{ ansible_eth1.ipv4.address }}",
        app: {
            port: 3001,
            host: "{{ ansible_eth1.ipv4.address }}"
        },
        rpc: {
            port: 4001,
            host: "{{ ansible_eth1.ipv4.address }}"
        },
        db_host: 'mongodb://mongodb1/',
        db_name: 'hive-dev',
        mail_server: {
            host: 'localhost',
            port: 25,
            from: "mail@hive.com",
            base_url: "https://107.170.160.123"
        },
        redis_port: '6379',
        redis_host: 'redis1',
        // password recovering
        hash_link_expire: 60 * 60 * 2, // seconds
        session_max_age: 1000 * 60 * 60 * 24 * 30,
        admin_roles: ['manager', 'support', 'admin'],
        uid_key: 'uid_autoincrement',
        // pusher socket.io settings
        pusher_origins : '*:*',
        pusher_heartbeats: false,
        events: events,
        feedUserTypes: feedUserTypes
    }
};